package com.somospnt.trepo;

import com.somospnt.trepo.repository.MemoRepository;
import static org.hamcrest.CoreMatchers.containsString;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class TrepoApplicationTests {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private MemoRepository memoRepository;

    @Before
    public void deleteAllBeforeTests() throws Exception {
        memoRepository.deleteAll();
    }

    @Test
    public void obtenerLinkAlRecursoMemo() throws Exception {

        mockMvc.perform(get("/"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$._links.memo").exists());
    }

    @Test
    public void crearMemo() throws Exception {

        mockMvc.perform(post("/memo").content(
                "{\"contenido\":\"Mongo es bueno\", \"autor\":\"Frodo\", \"tag\":\"mongo\"}"))
                .andExpect(status().isCreated())
                .andExpect(header().string("Location", containsString("memo/")));
    }

    @Test
    public void obtenerLinkABusquedaPorTag() throws Exception {

        mockMvc.perform(get("/memo/search"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$._links.findByTagIn").exists());
    }

    @Test
    public void obtenerMemoPorTag() throws Exception {

        mockMvc.perform(post("/memo").content(
                "{\"contenido\":\"Mongo es bueno\", \"autor\":\"Frodo\", \"tag\":\"mongo\"}"))
                .andExpect(status().isCreated())
                .andReturn();
        mockMvc.perform(post("/memo").content(
                "{\"contenido\":\"Data Rest es util\", \"autor\":\"Polo\", \"tag\":\"spring-data\"}"))
                .andExpect(status().isCreated())
                .andReturn();
        mockMvc.perform(post("/memo").content(
                "{\"contenido\":\"JPA es duro\", \"autor\":\"Ricon\", \"tag\":\"jpa\"}"))
                .andExpect(status().isCreated())
                .andReturn();

        mockMvc.perform(get("/memo/search/findByTagIn?tags=spring-data,jpa"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$._embedded.memo.[0].contenido").value("Data Rest es util"))
                .andExpect(jsonPath("$._embedded.memo.[0].autor").value("Polo"))
                .andExpect(jsonPath("$._embedded.memo.[1].contenido").value("JPA es duro"))
                .andExpect(jsonPath("$._embedded.memo.[1].autor").value("Ricon"));
    }
}
