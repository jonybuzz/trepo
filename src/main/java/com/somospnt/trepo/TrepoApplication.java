package com.somospnt.trepo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TrepoApplication {

	public static void main(String[] args) {
		SpringApplication.run(TrepoApplication.class, args);
	}
}
