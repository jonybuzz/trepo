package com.somospnt.trepo.repository;

import com.somospnt.trepo.domain.Memo;
import java.util.List;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "memo", path = "memo")
public interface MemoRepository extends MongoRepository<Memo, String> {

    List<Memo> findByTagIn(@Param("tags") List<String> tags);

}
